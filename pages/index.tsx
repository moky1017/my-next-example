import Head from "next/head";
import styled from "styled-components";
import MyCards from "./myCards";
// import MyTest from "./myTest";

import axios from "axios";
// import { ICard } from "@/components/type";
// import { useEffect, useState } from "react";
import  cardImages  from "@/components/cardImages";
import { ICard } from "@/components/type";

const Container = styled.div`
  padding: 0 0.5rem;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
  height: 100vh;
  min-height: 100vh;
`;

const Main = styled.main`
  padding: 5rem 0;
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export default function Home() {
  console.log("cardImages---", cardImages);

  return (
    <Container>
      <Head>
        <title>Build a Memory Game with React </title>
        <meta name="description" content="Build a Memory Game with React " />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Main>
        {/* <MyTest></MyTest> */}
        <MyCards cardImages={cardImages as ICard[]} />
      </Main>
    </Container>
  );
}
