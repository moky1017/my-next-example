import { useEffect, useState } from "react";

export default function MyTest() {
  const [isOpen, setIsOpen] = useState(false);
  const [str, setStr] = useState("close");

  useEffect(() => {
    if (isOpen) {
      setStr("open");
    } else {
      setStr("close");
    }
  }, [isOpen]);

  return (
    <div>
      <button
        onClick={() => setIsOpen((e) => !e)}
        style={{ color: "#fff", backgroundColor: "green", padding: "1rem" }}
      >
        onClick
      </button>
      <div style={{ color: "red", padding: "2rem" }}>{str}</div>
    </div>
  );
}
