/** @type {import('next').NextConfig} */
const execSync = require("child_process").execSync;

const lastCommitCommand = "git rev-parse HEAD";
const nextConfig = {
  // output: "export",
  async generateBuildId() {
    return execSync(lastCommitCommand).toString().trim();
  },
};

module.exports = nextConfig;
