import { useEffect, useState } from "react";
import { ICard } from "@/components/type";
import { MyApp, Button, CardGrid } from "@/components/myCardsStyle";
import SingleCardModel from "@/components/SingleCardModel";

export default function MyCards({ cardImages }: { cardImages: ICard[] }) {
  const [cards, setCards] = useState<ICard[]>([]);
  const [turns, setTurns] = useState(0);
  const [choiceOne, setChoiceOne] = useState<ICard | null>(null);
  const [choiceTwo, setChoiceTwo] = useState<ICard | null>(null);
  const [disabled, setDisabled] = useState(false);

  const handleChoice = (card: ICard) => {
    choiceOne ? setChoiceTwo(card) : setChoiceOne(card);
  };

  useEffect(() => {
    if (choiceOne && choiceTwo) {
      setDisabled(true);
      if (choiceOne.src === choiceTwo.src) {
        setCards((prevCard: ICard[]) => {
          return prevCard.map((card: ICard) => {
            if (card.src === choiceOne.src) {
              return { ...card, matched: true };
            } else {
              return card;
            }
          });
        });
        reseTurn();
      } else {
        setTimeout(() => reseTurn(), 1000);
      }
    }
  }, [choiceOne, choiceTwo]);

  const reseTurn = () => {
    setChoiceOne(null);
    setChoiceTwo(null);
    setTurns((precTurns) => precTurns + 1);
    setDisabled(false);
  };

  const shuffledCards = () => {
    const shuffledCards: ICard[] = [...cardImages, ...cardImages]
      .sort(() => Math.random() - 0.5)
      .map((card: ICard) => ({ ...card, id: Math.random() }));
    setChoiceOne(null);
    setChoiceTwo(null);
    setCards(shuffledCards);
    setTurns(0);
  };

  useEffect(() => {
    shuffledCards();
  }, []);

  return (
    <MyApp>
      <h1>Build a Memory Game </h1>
      <Button onClick={shuffledCards}>New Game</Button>
      <div style={{ color: "#fff", margin: "1em 0" }}>turns:{turns}</div>
      <CardGrid>
        {cards.map((card: ICard, index: number) => (
          <SingleCardModel
            key={index}
            card={card}
            flipped={card === choiceOne || card === choiceTwo || card.matched}
            handleChoice={handleChoice}
            disabled={disabled}
          />
        ))}
      </CardGrid>
    </MyApp>
  );
}
