const cardImages = [
  { src: "/268-200x300.jpg", matched: false },
  { src: "/270-200x300.jpg", matched: false },
  { src: "/491-200x300.jpg", matched: false },
  { src: "/1048-200x300.jpg", matched: false },
];

export default cardImages;
