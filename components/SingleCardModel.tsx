import { ICard } from "./type";
import { Card } from "./singleCardStyle";
import Image from "next/image";

interface IProps {
  card: ICard;
  handleChoice: (card: ICard) => void;
  flipped: boolean;
  disabled: boolean;
}

export default function SingleCardModel(props: IProps) {
  const { card, handleChoice, flipped, disabled } = props;

  const handleClick = () => {
    if (!disabled) {
      handleChoice(card);
    }
  };

  return (
    <Card>
      <div className={flipped ? "flipped" : ""}>
        <Image
          width={200}
          height={300}
          className="front"
          src={card.src}
          alt="front"
        />
        <Image
          width={200}
          height={300}
          className="back"
          src="cardFace_200x300.svg"
          alt="back"
          onClick={handleClick}
        />
      </div>
    </Card>
  );
}
