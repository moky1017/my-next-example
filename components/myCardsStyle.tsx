import styled from "styled-components";

export const MyApp = styled.div`
  font-family: sans-serif;
  text-align: center;
`;

export const Button = styled.button`
  background: gray;
  border: 2px solid #fff;
  padding: 6px 12px;
  border-radius: 4px;
  color: #fff;
  cursor: pointer;
  font-size: 1em;
  &:hover {
    background: #c23866;
    color: #fff;
  }
`;

export const CardGrid = styled.div`
  margin-top: 40px;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-gap: 20px;
`;
