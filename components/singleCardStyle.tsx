import styled from "styled-components";

export const Card = styled.div`
  position: relative;
  img {
    // width: 100%;
    display: block;
    border-radius: 10px;
  }

  .front {
    transform: rotateY(90deg);
    transition: all ease-in 0.2s;
    position: absolute;
  }
  .back {
    transition: all ease-in 0.2s;
    transition-delay: 0.2s;
  }

  .flipped {
    .front {
      transform: rotateY(0deg);
      transition-delay: 0.2s;
    }
    .back {
      transform: rotateY(90deg);
      transition-delay: 0s;
    }
  }
`;
